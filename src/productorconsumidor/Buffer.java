/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package productorconsumidor;

import monitor.AbstractMonitor;
import monitor.Condition;

public class Buffer extends AbstractMonitor
{ private int numSlots = 0, cont = 0;   
  private double[] buffer = null;
  private Condition deposita = makeCondition();
  private Condition extrae= makeCondition();
  
  public Buffer( int p_numSlots ) 
  { numSlots = p_numSlots ; 
    buffer = new double[numSlots] ;
  }
  public void depositar( double valor ) throws InterruptedException
  {
    enter();
    if(cont == numSlots ) deposita.await();
    buffer[cont] = valor; cont++;      
    if(!extrae.isEmpty()) extrae.signal();
    leave();
  }
  public double extraer() throws InterruptedException
  {
    enter();
    double valor;
    if( cont == 0 ) extrae.await();
    cont--; valor = buffer[cont];
    if(!deposita.isEmpty()) deposita.signal();
    leave();
    return valor;    
  }

}
