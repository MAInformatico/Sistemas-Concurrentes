/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package barberodurmiente;
import monitor.AbstractMonitor;
import monitor.Condition;

/**
 *
 * @author mike
 */
public class Barberia extends AbstractMonitor{
    private Condition sillaLibre; //indica si hay un cliente en la silla pelandose
    private Condition barberoDormido; //indica el estado del barbero
    private Condition listaEspera; //indica si hay cola para pelarse
    private boolean sillaDisponible;    
    private int cola;
    public Thread thr;
    
    Barberia(){
        barberoDormido=makeCondition();
        listaEspera=makeCondition();
        sillaLibre=makeCondition();
    }
        
    public void cortarPelo(){
        enter();
        if(sillaDisponible==true){ //nadie pelandose
            sillaLibre.await();
            System.out.println("Cliente: Me estoy pelando\n");
            barberoDormido.signal();
        }
        leave();
    }
    
    public void siguienteCliente(){
        enter();
        if(sillaDisponible==true && cola==0){
            listaEspera.await();
            System.out.println("Barbero: ¡Qué pase el siguiente!\n");
            sillaLibre.signal();
        }
        leave();
    }
    
    public void finCliente(){
        enter();        
        barberoDormido.await();
        System.out.println("Barbero: He acabado de pelar a un cliente\n");
        listaEspera.signal();
        leave();
    }    
}
