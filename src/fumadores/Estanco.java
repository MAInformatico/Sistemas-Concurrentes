/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fumadores;
import monitor.*;

public class Estanco extends AbstractMonitor {
    private int miIngrediente;
    private int ingrediente;
    private boolean hayIngrediente;
    private Condition conditionEst;
    private Condition Fuma;
    private Condition esperaRecogida;
    public String ingredientegenerado[]={"TABACO", "PAPEL", "CERILLAS"};

    public Estanco() {
       Fuma= makeCondition();
       conditionEst= makeCondition();
       esperaRecogida=makeCondition();
    }
    
    public void obtenerIngrediente(int ingre){
        enter();
        while(!hayIngrediente || miIngrediente !=ingre)
            Fuma.await();
            System.out.println("Fumador fumando: " + ingredientegenerado[ingre]);
            hayIngrediente=false;
            conditionEst.signal();
        leave();
    }
        
    public void ponerIngrediente(int ingrediente){
        enter();
        while(hayIngrediente){
            conditionEst.await();
        }
        hayIngrediente=true; //Tenemos un ingrediente generado
            Fuma.signal();
        leave();
    }
    
    public void esperarRecogidaIngrediente(){
        enter();
        while(hayIngrediente==true){
            esperaRecogida.await();
        }
        hayIngrediente=false; //Un fumador ha cogido el ingrediente
            conditionEst.signal();
        leave();
    }
    
    
    
    
}
