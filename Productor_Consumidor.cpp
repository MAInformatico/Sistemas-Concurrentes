#include <iostream>
#include <pthread.h>
#include <semaphore.h>

using namespace std;

sem_t prod;
sem_t cons;
#define num_items 75 //constante num_items con un valor entre 50 y 100
#define tam_vec 75 //definir el vector intermedio (buffer) con capacidad fija prestablecida en
//condición: tam_vec < num_item
//siempre nº de celdas ocupadas será un numero entre 0 y tam_vec
unsigned int primera_libre=0; //Solución LIFO, se incrementa al escribir y se decrementa al leer
int vector[tam_vec]={0}; //el buffer

int producir_dato(){  
  static int contador = 1;
  return contador ++;
}

void consumir_dato(int dato){
  cout << "dato recibido: " << dato << endl;  
}

void *  productor(void *){
  for(unsigned i=0; i<num_items; i++){
    sem_wait(&prod);
    int dato= producir_dato();
    //insertar dato en el vector
    vector[i]=dato;
    cout << "Se ha ejecutado el proceso productor\n";
    sem_post(&cons);
    primera_libre++;
  }
  return NULL;  
}

void entradasOcupadasyLibres(){
    for(int i=0; i<=tam_vec; i++){
      if(i==primera_libre){
	cout << "Posicion de la primera celda libre: " << i <<  endl;
      }
    }  
}

void * consumidor(void *){
  for(unsigned i=0; i<num_items;i++){
      sem_wait(&cons);
      int dato;
      //leer "dato" desde el vector intermedio
      dato=vector[i];
      consumir_dato(dato);
      cout << "Se ha ejecutado el proceso consumidor\n";
      sem_post(&prod);
      primera_libre--;
  }
  return NULL;
}

int main(){
  sem_init(&prod,0,1); //inicializamos el semaforo del productor a 1
  sem_init(&cons,0,0); //inicilizamos el semaforo del consumidor a 0
  pthread_t  t1,t2;
  //creamos las hebras
  pthread_create(&t1,NULL,productor,NULL);
  //pthread_join(t1,NULL); //esperamos a que finalice la 1ª hebra
  pthread_create(&t2,NULL,consumidor,NULL);
  pthread_exit(NULL);
  cout << "Fin " << endl;
}