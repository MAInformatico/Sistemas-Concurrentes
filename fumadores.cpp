#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <iostream>
using namespace std;
//semaforos para los procesos fumadores
sem_t S0;
sem_t S1;
sem_t S2;
//semaforo para el proceso estanquero
sem_t Se;

void fumar(){
  cout << "Fumando ...\n";
  sleep(1); //lo "dejamos fumando" 5 segundos
}

void * estanquero(void *){ //función que ejecutara el proceso estanquero
  while(true){
    sem_wait(&Se);
    int ingrediente= rand()%3; //generamos un ingrediente aleatoriamente
    if(ingrediente==0){
      cout << "Ingrediente 0\n";
      sem_post(&S0);   
    }
    else if(ingrediente==1){
      cout << "Ingrediente 1\n";
      sem_post(&S1);
    }
    else{
      cout << "Ingrediente 2\n";
      sem_post(&S2);
    }
  }
}

void * fumador(void * arg){ //funcion que ejecutaran los procesos fumadores
  while(true){
    int ingrediente= (unsigned long) arg;
    if(ingrediente==0){
      cout << "Fumador de ingrediente 0\n";
      sem_wait(&S0);   
    }
    else if(ingrediente==1){
      cout << "Fumador de ingrediente 1\n";
      sem_wait(&S1);
    }
    else{
      cout << "Fumador de ingrediente 2\n";
      sem_wait(&S2);
    }
   sem_post(&Se);
   fumar(); 
  }
}

int main(){
  sem_init(&S0,0,0);
  sem_init(&S1,0,0);
  sem_init(&S2,0,0);
  sem_init(&Se,0,1); //semaforo del estanquero
  pthread_t fuma1; 
  pthread_t fuma2; 
  pthread_t fuma3; 
  pthread_t estan; 
  pthread_create(&estan,NULL,estanquero,NULL); //creamos el proceso estanquero
  pthread_create(&fuma1,NULL,fumador,(void *)0); //creamos los procesos fumadores
  pthread_create(&fuma2,NULL,fumador,(void *)1); 
  pthread_create(&fuma3,NULL,fumador,(void *)2); 
  pthread_exit(NULL);  
}